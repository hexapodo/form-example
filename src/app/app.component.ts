import { Component, OnInit } from '@angular/core';
import { FormControl, Validators, FormGroup, FormArray } from '@angular/forms';


@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit{

    name = new FormControl('', [Validators.required]);

    position = new FormControl('', [Validators.required]);

    positionTime = new FormControl(null, [Validators.required]);

    // Group
    job = new FormGroup({
        position: this.position,
        positionTime: this.positionTime
    });

    // Array of formcontrols
    historyJobs = new FormArray([
        new FormControl('', [Validators.required]),
        new FormControl('', [Validators.required])
    ]);

    // Array of formgroup
    studies = new FormArray([
        new FormGroup({
            entity: new FormControl('', [Validators.required]),
            year: new FormControl('', [Validators.required])
        }),
        new FormGroup({
            entity: new FormControl('', [Validators.required]),
            year: new FormControl('', [Validators.required])
        })
    ]);

    form = new FormGroup({
        name: this.name,
        job: this.job,
        historyJobs: this.historyJobs,
        studies: this.studies
    });

    ngOnInit(): void {
        console.log(this.studies);
    }

    onSubmit(): void {
        console.log(this.form.value);
    }

    deatach(): void {
        this.form.removeControl('job');
    }

    atach(): void {
        this.form.addControl('job', this.job);
    }

}
